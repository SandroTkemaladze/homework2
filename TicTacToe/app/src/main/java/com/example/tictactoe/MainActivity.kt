package com.example.tictactoe

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private var firstPlayer = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init() {
        button00.setOnClickListener {
            buttonChange(button00)
        }
        button01.setOnClickListener {
            buttonChange(button01)
        }
        button02.setOnClickListener {
            buttonChange(button02)
        }
        button10.setOnClickListener {
            buttonChange(button10)
        }
        button11.setOnClickListener {
            buttonChange(button11)
        }
        button12.setOnClickListener {
            buttonChange(button12)
        }
        button20.setOnClickListener {
            buttonChange(button20)
        }
        button21.setOnClickListener {
            buttonChange(button21)
        }
        button22.setOnClickListener {
            buttonChange(button22)
        }
    }

    private fun buttonChange(button: Button) {
        if (firstPlayer) {
            button00.text = "X"
        } else {
            button00.text = "0"
        }
        button00.isClickable = false
        firstPlayer = !firstPlayer
        checkWinner()
    }

    private fun checkWinner() {
        if (button00.text.toString().isNotEmpty() && button00.text.toString() == button01.text.toString() && button01.text.toString() == button02.text.toString()

        showToast(button00.text.toString())) {
            else if (button10.text.toString()
                    .isNotEmpty() && button10.text.toString() == button11.text.toString() && button11.text.toString() == button02.text.toString()
            )
                Toast.makeText(this, "Winner is ${button00.text.toString()}", Toast.LENGTH_SHORT)
                    .show()
        }
    }
}